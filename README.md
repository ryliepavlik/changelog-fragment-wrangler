# Changelog Fragment Wrangler

<!--
Copyright 2024, Collabora, Ltd.

SPDX-License-Identifier: CC-BY-4.0
-->

A tool primarily for internal usage by Rylie Pavlik.

A web app for manipulating changelog fragments a la [Proclamation](https://proclamation.readthedocs.io/).

## Expectations

- Run using Poetry (in a virtual environment)
- Expects to be cloned next to a Monado clone.
  - Change this by setting environment variable `FRAGMENT_WRANGLER_SETTINGS` to
    the absolute path of a Python module that replaces/overrides
    `changelog_fragment_wrangler/default_settings.py`

## Setup

```sh
poetry install
```

## Running

```sh
poetry run flask --app changelog_fragment_wrangler run
```

or, for development purposes,

```sh
poetry run flask --app changelog_fragment_wrangler run --debug
```
