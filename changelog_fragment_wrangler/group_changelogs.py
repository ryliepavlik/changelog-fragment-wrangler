#!/usr/bin/env python3
# Copyright 2024, Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0
#
# Original author: Rylie Pavlik <rylie.pavlik@collabora.com>

import re
from dataclasses import dataclass, field
from pathlib import Path

from jinja2 import Environment, FileSystemLoader

_ROOT = Path(__file__).parent.parent.resolve()

# Manually populated from output, fairly special purpose stopword list
_STOPWORDS = {
    "a",
    "add",
    "added",
    "all",
    "also",
    "and",
    "are",
    "as",
    "be",
    "been",
    "been",
    "both",
    "but",
    "by",
    "can",
    "can",
    "code",
    "do",
    "fix",
    "for",
    "from",
    "has",
    "have",
    "if",
    "improve",
    "in",
    "info",
    "into",
    "is",
    "it",
    "main",
    "make",
    "more",
    "mr",
    "needed",
    "new",
    "no",
    "not",
    "now",
    "of",
    "on",
    "one",
    "only",
    "or",
    "out",
    "set",
    "so",
    "some",
    "that",
    "the",
    "this",
    "tidy",
    "to",
    "turn",
    "use",
    "used",
    "various",
    "vk",
    "was",
    "we",
    "when",
    "where",
    "which",
    "with",
}


def _strip_front_matter(lines: list[str]):
    front_matter_delimiter = "---"
    if lines[0].strip() == front_matter_delimiter:
        lines.pop(0)
        while lines and lines[0].strip() != front_matter_delimiter:
            lines.pop(0)


_WORD_RE = re.compile(r"[a-z][a-zA-Z0-9]+\b")


def _iter_words_in_lines(lines: list[str]):
    for line in lines:
        for m in _WORD_RE.finditer(line.casefold()):
            m_str = m.group()
            if m_str not in _STOPWORDS:
                yield m_str


@dataclass
class Cluster:
    word: str
    filenames: list[Path] = field(default_factory=list)

    @property
    def number_of_filenames(self):
        return len(self.filenames)


class WordFrequencyAccumulator:
    def __init__(self) -> None:
        self.words: dict[str, Cluster] = dict()
        self.filenames: list[Path] = list()

        self.file_contents: dict[Path, str] = dict()

    def process_file(self, relative: Path, filename: Path):
        with open(filename, "r") as fp:
            lines = list(fp)
        _strip_front_matter(lines)
        if not lines:
            print(f"Warning: {relative} seems empty after removing front matter!")
            return

        with open(filename, "r") as fp:
            self.file_contents[relative] = fp.read().strip()

        file_words = set(_iter_words_in_lines(lines))
        for word in file_words:
            if word not in self.words:
                self.words[word] = Cluster(word)

            self.words[word].filenames.append(relative)

    def get_results(self) -> list[Cluster]:
        return list(
            sorted(
                self.words.values(),
                key=lambda cluster: cluster.number_of_filenames,
                reverse=False,
            )
        )


def load_all(repo_dir, changelog_dir: Path) -> WordFrequencyAccumulator:
    acc = WordFrequencyAccumulator()

    for fn in changelog_dir.glob("*/*.md"):
        relative = fn.relative_to(repo_dir)
        print(relative)
        acc.process_file(relative, fn)

    return acc


def print_results(acc: WordFrequencyAccumulator):
    results = acc.get_results()

    for cluster in results:
        print(f"{cluster.word}: {cluster.number_of_filenames}")


def results_to_html(acc: WordFrequencyAccumulator):
    env = Environment(loader=FileSystemLoader(Path(__file__).parent))
    template = env.get_template("group_changelogs.html.j2")
    clusters = [c for c in reversed(acc.get_results()) if c.number_of_filenames > 1]  #

    html = template.render({"clusters": clusters[:10], "files": acc.file_contents})

    with open("changelog_word_groups.html", "w") as fp:
        fp.writelines(html)


if __name__ == "__main__":
    changelog_dir = _ROOT / "doc" / "changes"
    acc = load_all(_ROOT, changelog_dir)
    print_results(acc)

    results_to_html(acc)
