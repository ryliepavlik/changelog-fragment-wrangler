#!/usr/bin/env python3
# Copyright 2024, Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0
#
# Original author: Rylie Pavlik <rylie.pavlik@collabora.com>

from pathlib import Path
from proclamation.types import ReferenceParser
from proclamation.merge import merge_fragments
from flask import redirect, render_template, request, url_for

from . import app
from .group_changelogs import WordFrequencyAccumulator

_PROJECT = app.config["PROJECT"]
_CHANGELOGS = app.config["CHANGELOGS"]
_LIMIT_CLUSTERS = app.config["LIMIT_CLUSTERS"]

app.logger.info("PROJECT: %s", str(_PROJECT))
app.logger.info("CHANGELOGS: %s", str(_CHANGELOGS))


def load_all(repo_dir, changelog_dir: Path) -> WordFrequencyAccumulator:
    acc = WordFrequencyAccumulator()
    parser = ReferenceParser()
    for file_path in changelog_dir.glob("*/*.md"):
        relative = file_path.relative_to(repo_dir)
        parsed = parser.parse_filename(file_path.name)
        if not parsed:
            app.logger.info("Skipping file %s", relative)
        acc.process_file(relative, file_path)

    return acc


@app.route("/")
def index():
    acc = load_all(_PROJECT, _CHANGELOGS)
    clusters = [c for c in reversed(acc.get_results()) if c.number_of_filenames > 1]  #

    if _LIMIT_CLUSTERS:
        clusters = clusters[:_LIMIT_CLUSTERS]

    return render_template(
        "group_changelogs_webapp.html", clusters=clusters, files=acc.file_contents
    )


def do_merge(src, dst):
    app.logger.info("Merging: %s %s", dst, src)
    full_dst = _PROJECT / dst
    full_src = _PROJECT / src
    merge_fragments([full_dst, full_src], None)


@app.post("/merge")
def merge():
    do_merge(request.form["src"], request.form["dst"])
    if "anchor" in request.form:
        return redirect(url_for("index") + "#" + request.form["anchor"])
    return redirect(url_for("index"))
