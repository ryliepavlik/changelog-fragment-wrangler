#!/usr/bin/env python3
# Copyright 2024, Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0
#
# Original author: Rylie Pavlik <rylie.pavlik@collabora.com>
from pathlib import Path

_root = Path(__file__).parent.parent.resolve()

# If you make your own config file based on this,
# in the root directory of changelog_fragment_wrangler,
# the following line will compute the repo root correctly instead.
# _root = Path(__file__).parent.resolve()


PROJECT = (_root.parent / "monado").resolve()
CHANGELOGS = PROJECT / "doc" / "changes"

# The max number of word-based clusters to show
LIMIT_CLUSTERS = 20
