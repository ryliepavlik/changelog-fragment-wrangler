// Copyright 2024 Collabora, Ltd.
//
// SPDX-License-Identifier: BSL-1.0

function drop(ev) {
  ev.preventDefault();
  const srcFn = ev.dataTransfer.getData("text/plain");
  //   const source = document.getElementById(srcFn);
  const dstFn = ev.target.parentElement.id;

  const wordAnchor =
    ev.target.parentElement.parentElement.parentElement.parentElement.id;
  console.log(
    "Dropped %s onto %s in %s",
    srcFn,
    ev.target.parentElement.id,
    wordAnchor
  );
  submitMerge(dstFn, srcFn, wordAnchor);

  // Clean up any lingering drop target highlighting
  Array.from(document.getElementsByClassName("droptarget")).forEach(
    (element) => {
      element.classList.remove("droptarget");
    }
  );
}

window.addEventListener("DOMContentLoaded", () => {
  Array.from(document.getElementsByClassName("filename")).forEach((element) => {
    element.addEventListener("dragstart", dragstart);
  });

  Array.from(document.getElementsByClassName("filename_target")).forEach(
    (element) => {
      element.addEventListener("dragover", dragover);
      element.addEventListener("drop", drop);
      element.addEventListener("dragenter", (ev) => {
        ev.target.classList.add("droptarget");
      });
      element.addEventListener("dragleave", (ev) => {
        if (ev.target == undefined) {
          return;
        }
        if (ev.target.classList == undefined) {
          return;
        }
        ev.target.classList.remove("droptarget");
      });
    }
  );
  if (document.location.hash != "") {
    const el = document.querySelector(document.location.hash);
    if (el != null) {
      el.setAttribute("open", "true");
    }
  }
});
