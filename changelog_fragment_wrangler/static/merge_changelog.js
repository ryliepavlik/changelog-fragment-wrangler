// Copyright 2024 Collabora, Ltd.
//
// SPDX-License-Identifier: BSL-1.0

function submitMerge(dstFn, srcFn, anchor) {
  // synchronous
  const form = document.createElement("form");
  form.method = "post";
  form.action = "/merge";
  // const hidden = document.createElement("input");
  // hidden.type = "hidden";
  // hidden.name = "payload";
  // hidden.value = post;
  // form.appendChild(hidden);
  {
    const hidden = document.createElement("input");
    hidden.type = "hidden";
    hidden.name = "src";
    hidden.value = srcFn;
    form.appendChild(hidden);
  }
  {
    const hidden = document.createElement("input");
    hidden.type = "hidden";
    hidden.name = "dst";
    hidden.value = dstFn;
    form.appendChild(hidden);
  }
  if (anchor != undefined) {
    const hidden = document.createElement("input");
    hidden.type = "hidden";
    hidden.name = "anchor";
    hidden.value = anchor;
    form.appendChild(hidden);
  }
  document.body.appendChild(form);
  form.submit();

  // async

  // const post = JSON.stringify({ src: srcFn, dst: dstFn });
  // let xhr = new XMLHttpRequest();
  // xhr.open("POST", document.location, true);
  // xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");
  // xhr.send(post);

  // xhr.onload = function () {
  //   if (xhr.status === 201) {
  //     console.log("Post successfully created! %s", post);
  //   }
  // };
}

function dragstart(ev) {
  // Transmit the element ID when we drag - this is the filename
  ev.dataTransfer.setData("text/plain", ev.target.id);
  ev.dataTransfer.effectAllowed = "move";

  console.log("dragged %s", ev.target.id);
}

function dragover(ev) {
  ev.preventDefault();
  ev.dataTransfer.dropEffect = "move";
  const source = document.getElementById(ev.dataTransfer.getData("text/plain"));
}
