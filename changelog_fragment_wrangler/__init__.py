#!/usr/bin/env python3
# Copyright 2024, Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0
#
# Original author: Rylie Pavlik <rylie.pavlik@collabora.com>

import logging

from flask import Flask

app = Flask(__name__, instance_relative_config=True)


app.logger.setLevel(logging.INFO)  # Set log level to INFO

app.config.from_object("changelog_fragment_wrangler.default_settings")
app.config.from_envvar("FRAGMENT_WRANGLER_SETTINGS", silent=True)

# handler = logging.FileHandler("app.log")  # Log to a file
# app.logger.addHandler(handler)

from . import grouped_webapp_views  # noqa: F401,E402
